﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyHowest
{
    public static class MyLibExtensions
    {
        public static string Reverse(this string s)
        {
            string newString = string.Empty;
            for (int i = s.Length-1; i>=0; i--)
            {
                newString += s[i];
            }
            return newString;
        }
    }
}
