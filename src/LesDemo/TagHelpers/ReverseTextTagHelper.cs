﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.Runtime.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using MyHowest;

namespace LesDemo.TagHelpers
{
    // You may need to install the Microsoft.AspNetCore.Razor.Runtime package into your project
    //[HtmlTargetElement("tag-name")]
    public class ReverseTextTagHelper : TagHelper
    {
        public string Keyword { get; set; }

        public bool Caps { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "div";
            output.TagMode = TagMode.StartTagAndEndTag;

            string result = Keyword.Reverse();
            if (Caps) {
                result = result.ToUpper();
            }
            output.Content.SetContent(result);
        }
    }
}
